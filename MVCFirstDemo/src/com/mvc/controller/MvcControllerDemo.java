package com.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
//@RequestMapping("/mcvtest")// class level mapping 
public class MvcControllerDemo {
	
	@RequestMapping("/welcome") // method level mapping 
	public ModelAndView helloWorld() {
		System.out.println("This is in FirstMVCDemo helloWorld=========");
 
        String disPlayData="This is my First Spring MVC Example!!!!";
        
		return new ModelAndView("welcome", "message", disPlayData);
	}

}
